
import paramiko
import os


class MySFTPClient(paramiko.SFTPClient):
    def put_dir(self, source, target):
        ''' Uploads the contents of the source directory to the target path. The
            target directory needs to exists. All subdirectories in source are
            created under target.
        '''
        for item in os.listdir(source):
            if os.path.isfile(os.path.join(source, item)):
                self.put(os.path.join(source, item), '%s/%s' % (target, item))
            else:
                self.mkdir('%s/%s' % (target, item), ignore_existing=True)
                self.put_dir(os.path.join(source, item), '%s/%s' % (target, item))

    def mkdir(self, path, mode=511, ignore_existing=False):
        ''' Augments mkdir by adding an option to not fail if the folder exists  '''
        try:
            super(MySFTPClient, self).mkdir(path, mode)
        except IOError:
            if ignore_existing:
                pass
            else:
                raise


def sftp_upload_file(host, user, password, source_folder, local_folder, port=22, timeout=10):
    print(source_folder, local_folder)
    """
        Upload file, can not upload directory.
        :param host: sftp server host name or ip.
        :param port: sftp server listening port nubmer.
        :param user: sftp user name
        :param password: sftp account password
        :param server_path: remote server file path，for example：/root/test/test.txt
        :param local_path: local file path (c:/test.txt)
        :param timeout: upload connection timeout number ( an integer value, default is 10 )
        :return: bool
    """

    try:
        transport = paramiko.Transport((host, port))
        transport.connect(username=user, password=password)
        sftp = MySFTPClient.from_transport(transport)
        sftp.mkdir(source_folder, ignore_existing=True)
        sftp.put_dir(local_folder, source_folder)
        sftp.close()
        return True
    except Exception as e:
        print(e)
        return False


def sftp_down_file(host, user, password, server_path, local_path, port=22, timeout=10):
    """
       Download file, do not support download directory.
       :param host: SFTP server host name or ip address.
       :param port: SFTP server port number, the default port number is 22.
       :param user: SFTP server user name.
       :param password: SFTP server password.
       :param server_path: Download file path on server side.
       :param local_path: Local file path, download file saved as.
       :param timeout: Connection time out must be an integer number, defautl value is 10.
       :return: bool
    """

    try:
        # Create the transport object
        t = paramiko.Transport((host, port))

        # Set connection timeout value.
        t.banner_timeout = timeout

        # Connect to the SFTP server use username and password.
        t.connect(username=user, password=password)

        # Get SFTP client object.
        sftp = paramiko.SFTPClient.from_transport(t)

        # Download file from server side and save it to local path.
        sftp.get(server_path, local_path)

        # Close SFTP server connection.
        t.close()
        return True
    except Exception as e:
        print(e)
        return False
