from configparser import ConfigParser

cfg = ConfigParser()
cfg.read('config.ini')


def config_parser():
    component_wrt_branch = ["".join(option.strip("-")) for option in cfg["SERVER"]]
    return component_wrt_branch


