import os
import os.path
import csv
import git
import copy
import datetime
import server_config_parser
from ssh_client import sftp_down_file, sftp_upload_file

# https://<gitlabusername>@gitlab.com/gitlab_user/myrepo.git

remote_repo = ""
local_repo = ""
repo_name = ""
server_csv_rows = None
component_csv_rows = None
username = ""
server_path = "temp"
application_path = r'D:\Pro_orders\repo_deployment'


# https://gitlab.com/tasaqipro/test.git


def pull_repo(l_repo):
    repo = git.Repo(l_repo)
    o = repo.remotes.origin
    o.pull()


def authentication():
    global remote_repo, local_repo, username, repo_name
    repo_url = input("Enter Project Repo URL : ")
    username = input("Enter your Gitlab Username : ")

    repo_url = repo_url.split("//")[1]
    repo_name = repo_url.split("/")[-1].split(".")[0]

    local_repo = os.path.join(os.getcwd(), repo_name)
    remote_repo = f"https://{username}@{repo_url}"

    try:
        print("-----------------------------------------------------------------------------------")
        print("Please wait for Authentication ......... ")
        if os.path.isdir(local_repo):
            pull_repo(local_repo)
            print("Local Repo is successfully Update")

        else:
            print(f"Project with {repo_name} is Not Exist, Clone the URL")
            home_dir = os.system(f"git clone {remote_repo}")

        return True

    except Exception as e:
        print("Authentication Failed... ", e, " Retry after some time")
        return False


def have_server_data():
    global server_csv_rows
    for root, dirs, files in os.walk(local_repo):
        if "server.csv" in files:
            with open(os.path.join(root, "server.csv"), "r") as csv_file:
                server_csv_reader = csv.reader(csv_file)
                server_csv_rows = [row for row in server_csv_reader]
            return True
    return False


def have_component_data():
    global component_csv_rows
    for root, dirs, files in os.walk(local_repo):
        if "components.csv" in files:
            with open(os.path.join(root, "components.csv"), "r") as csv_file:
                component_csv_reader = csv.reader(csv_file)
                component_csv_rows = [row for row in component_csv_reader]
            return True
    return False


def update_server_csv(update_server_data, branch):
    os.system("git checkout main")
    with open(os.path.join(local_repo, "server.csv"), "w", newline='') as csv_file:
        component_csv_writer = csv.writer(csv_file)
        for row in update_server_data:
            component_csv_writer.writerow(row)
    try:
        print("-----------------------Git Actions --------------------------------")
        print(os.getcwd())
        print("Adding changes ")
        os.system("git add . ")
        print("commit changes")
        os.system(r'git commit -m "new updates"')
        print("pushing data on remote repository ")
        os.system(f"git push ")

    except Exception as e:
        print('Some error occured while pushing the code', e)


def update_server(active_branch, sha_value, server_name):
    today = datetime.datetime.now()
    # yyyy-mm-dd hh:mm:ss.s
    current_datetime = f"{today.year}-{today.month}-{today.day} {today.hour}-{today.minute}-{today.second}"
    print("===== > updating server")

    update_server_copy = copy.deepcopy(server_csv_rows)

    for i, row in enumerate(server_csv_rows):
        if i < 1:
            continue

        if server_name == row[0]:
            server_ip = row[1]
            server_username = row[2]
            server_password = row[3]
            last_user = username

            print(row)

            update_server_copy[i][4] = last_user
            update_server_copy[i][5] = current_datetime
            update_server_copy[i][6] = sha_value
            update_server_copy[i][7] = active_branch

            is_download = sftp_down_file(server_ip, server_username, server_password,
                                         os.path.join(server_path, "config.ini"),
                                         os.path.join(application_path, "config.ini"))

            if is_download:
                remote_server_config = server_config_parser.config_parser()
                for cc, com in enumerate(component_csv_rows):
                    if cc < 1:
                        continue

                    local_components, branch = [c.lower() for c in str(com[-1]).split(",")], com[0]
                    is_valid_server = True
                    for remote_comp in remote_server_config:
                        if remote_comp not in local_components:
                            is_valid_server = False
                    print("-------------------------------------------------------------------------")
                    print(branch, is_valid_server, local_components, remote_server_config)
                    if active_branch == branch:
                        if is_valid_server:
                            # Ask for permission to user
                            print("-------------------------------------------------------------------------")
                            is_premission = input("confirm changes (yes/y)? ")
                            if is_premission.lower() in ["yes", "y"]:
                                # start uploading files from local to remote repo
                                is_upload = sftp_upload_file(server_ip,
                                                             server_username,
                                                             server_password,
                                                             os.path.join(server_path, repo_name),
                                                             os.path.join(local_repo))
                                if is_upload:
                                    print(f"The Server with {server_ip} is successfully Update")
                                    update_server_csv(update_server_copy, active_branch)
                                else:
                                    print("Fail to upload !")
                            else:
                                print("Cancel Updates ! ")
                    else:
                        print(f"Found Another Branch = {branch} that satisfied the Componenet Condition ")
                        is_premission = input(
                            f"Do want to Update remote Server {server_ip} With branch {branch} (yes/y) for confirm changes ? ")
                        if is_premission.lower() in ["yes", "y"]:
                            # start uploading files from local to remote repo
                            is_upload = sftp_upload_file(server_ip,
                                                         server_username,
                                                         server_password,
                                                         os.path.join(server_path, repo_name),
                                                         os.path.join(local_repo))
                            if is_upload:
                                print(f"The Server with {server_ip} is successfully Update")
                                update_server_csv(update_server_copy, branch)
                            else:
                                print("Fail to upload !")
                        else:
                            print(f"Cancel TO push new Branch ! {branch} ")


def check_updates():
    os.chdir(local_repo)
    os.system("git pull --all")
    branches = os.popen('git branch -a').read()
    all_branches = [b.strip() for b in branches.split(" ")]
    all_branches = [b.split("/")[-1] for b in all_branches if "/" in b and "HEAD" not in b]

    # ==========================================================================================================
    # we get all branches
    all_branches = set(all_branches)
    print("All  branches : ", all_branches)

    """
     Switching branches, so that we will compare branch
     if active branch=last_branch, and last_sha value is not same, it means the branch is updated
    
    """
    is_update = True
    for active_branch in all_branches:
        repo = git.Repo(local_repo)
        repo.git.checkout(active_branch)
        commits = list(repo.iter_commits())

        # Find the latest commit -----------
        latest_commit = commits[-1]
        sha_value = latest_commit.hexsha
        print("------------------------------------------------------------------------")
        print("active_branch", active_branch)

        for i, row in enumerate(server_csv_rows):

            if i < 1:
                continue

            if sha_value == row[6] and active_branch == row[-1]:
                print(f"===>  recently updated, No new updates for {row[0]} ")
                is_update = False

            elif active_branch == row[-1]:
                print(f"===> new Updates on Server Name : {row[0]}  IP address: {row[1]} ")
                ask_for_updates = input(f"Do you want to Update Server {row[1]} ? (yes/y) ")
                if ask_for_updates.lower() in ["yes", "y"]:
                    update_server(active_branch, sha_value, row[0])

                else:
                    pass
            else:
                print(f" No updates for server {row[2]} ===> With respective to Branch {active_branch}")
                pass
    return is_update


def main():
    print("===============================================================")
    print("---------------------- welcome --------------------------------")
    print("===============================================================")

    if authentication():
        # Check server file
        if have_server_data():
            if have_component_data():
                # --------------------------- check updates of each server ----------------------------------------
                if check_updates():
                    pass
                else:
                    print("Error : No updates Available ")

            else:
                print("Error : No Component data Data found ")

        else:
            print("Error : No server Data found ")

    else:
        exit(1)


if __name__ == '__main__':
    main()
