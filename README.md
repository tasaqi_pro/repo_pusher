# Repo Pusher

What Is This?
-------------

This is a simple Python CLI application intended to provide a faclities to multiple branch server deployment


How To Use This
---------------


## Installation
* first ensure you have python globally installed in your computer. If not, you can get python [here](https://www.python.org").
* After doing this, confirm that you have installed virtualenv globally as well. If not, run this:
    ```bash
        $ pip install virtualenv
    ```
* Then, Git clone this repo to your PC
    ```bash
        $ git clone https://gitlab.com/tasaqi_pro/repo_pusher.git
    ```
  
* #### Dependencies
    1. Cd into your cloned repo as such:
        ```bash
            $ cd repo_pusher
        ```
    2. Create and fire up your virtual environment:
        ```bash
            $ virtualenv  venv -p python3
            $ source venv/bin/activate
        ```
    3. Install the dependencies needed to run the app:
        ```bash
            $ pip install -r requirements.txt

* #### Run It
    using this one simple command:
    ```bash
        $ python app.py
    ```



